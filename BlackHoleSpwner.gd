extends Node2D

const SPWN_DELAY = 1.0
const SPWN_DISTANCE = 7500
var spwn_timer = 0.0

@onready var black_hole = preload("res://black_hol.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	spwn_timer += delta
	
	if spwn_timer >= SPWN_DELAY:
		var bh_inst = black_hole.instantiate()
		add_child(bh_inst)
		var x_pos = randi_range(-SPWN_DISTANCE, SPWN_DISTANCE)
		bh_inst.position.x = x_pos
		spwn_timer = 0.0
