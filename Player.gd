extends CharacterBody2D

const SPEED = 2000.0
const ROTATION_SPEED = PI/2
var vel = Vector2(1, 0)*SPEED
var has_cheese = false
@onready var train_sprite = $Train
@onready var cheese_sprite = $TrainCheese

func _physics_process(delta):
	
	if Input.is_action_pressed("left"):
		vel = vel.rotated(delta*-ROTATION_SPEED)
		rotate(delta*-ROTATION_SPEED)
	
	elif Input.is_action_pressed("right"):
		vel = vel.rotated(delta*ROTATION_SPEED)
		rotate(delta*ROTATION_SPEED)
	
	velocity = vel
	move_and_slide()

func get_cheese():
	has_cheese = true
	show_cheese(true)
	
func lose_cheese():
	has_cheese = false
	show_cheese(false)

func show_cheese(bol):
	cheese_sprite.visible = bol
	train_sprite.visible = not bol
	
