extends Node2D

const SPEED = 1000


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.y += delta*SPEED


func _on_area_2d_body_entered(body):
	if body.is_in_group("Player"):
		body.queue_free()
